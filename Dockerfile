FROM docker:20.10
RUN adduser -h /home/ciuser -D -u 1000 -G wheel ciuser
WORKDIR /home/ciuser
COPY test/go.mod go.mod
COPY test/go.sum go.sum
COPY showVersions.sh /usr/local/bin/showVersions.sh

ENV GLIBC_VER=2.33-r0


# install glibc compatibility for alpine
# hadolint ignore=DL3018
RUN apk --no-cache add \
        go=1.15.12-r0 \
        binutils=2.35.2-r1 \
        curl=7.76.1-r0 \
        python3=3.8.10-r0 \
        groff=1.22.4-r1 \
        sudo=1.9.5p2-r0 \
    && chown ciuser /usr/local/bin/showVersions.sh && chmod +x /usr/local/bin/showVersions.sh \
    && curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-${GLIBC_VER}.apk \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk \
    && apk add --no-cache \
        glibc-${GLIBC_VER}.apk \
        glibc-bin-${GLIBC_VER}.apk \
    && curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
    && unzip awscliv2.zip \
    && aws/install \
    && rm -rf \
        awscliv2.zip \
        aws \
        /usr/local/aws-cli/v2/*/dist/aws_completer \
        /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index \
        /usr/local/aws-cli/v2/*/dist/awscli/examples \
    # Allow cisuser to sudo without password
    && sed -e 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' -i /etc/sudoers \
    # Download terratest and testify
    && go mod download \
    # Download and install Terraform
    && curl -sL https://releases.hashicorp.com/terraform/0.15.3/terraform_0.15.3_linux_amd64.zip -o terraform.zip \
    && unzip terraform.zip && mv terraform /usr/local/bin && rm terraform.zip \
    # Download and install Tflint
    && curl -sL https://github.com/terraform-linters/tflint/releases/download/v0.28.1/tflint_linux_amd64.zip -o tflint.zip \
    && unzip tflint.zip && mv tflint /usr/local/bin && rm tflint.zip && chmod ugo+x /usr/local/bin/tflint \
    # Download and install Terraform AWS Provider
    && curl -sL https://releases.hashicorp.com/terraform-provider-aws/3.39.0/terraform-provider-aws_3.39.0_linux_amd64.zip -o "aws_provider.zip" \
    && unzip aws_provider.zip && mkdir -p ~/.terraform.d/plugins && mv terraform-provider-aws_v3.39.0_x5 ~/.terraform.d/plugins && rm aws_provider.zip \
    && apk --no-cache del \
        curl \
    && rm glibc-${GLIBC_VER}.apk \
    && rm glibc-bin-${GLIBC_VER}.apk \
    && rm -rf /var/cache/apk/*
USER ciuser
ENTRYPOINT ["sh"]
CMD ["/usr/local/bin/showVersions.sh"]
