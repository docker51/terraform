module gitlab.com/bradthebuilder/docker51/terraform

go 1.14

require (
	github.com/gruntwork-io/terratest v0.34.5
	github.com/stretchr/testify v1.7.0
)
