package test

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/docker"
	"github.com/stretchr/testify/assert"
)

func TestDockerTerraform(t *testing.T) {
	// Sets tag from CI environment variable, if present
	CI_REGISTRY_IMAGE := os.Getenv("CI_REGISTRY_IMAGE")
	image := ""

	if CI_REGISTRY_IMAGE != "" {
		image = CI_REGISTRY_IMAGE + ":latest"
	} else {
		image = "registry.gitlab.com/docker51/terraform"
	}

	// Terratest's docker.Build's OtherOptions cannot accept "--cache-from"
	// buildOptions := &docker.BuildOptions{
	// 	Tags: []string{image},
	// 	BuildArgs: []string{"--cache-from " + image},
	// }

	// docker.Build(t, "../", buildOptions)

	// Manually invoke Docker build to save CI time
	s := "docker build --cache-from " + image + " --tag " + image + " ../"
	args := strings.Split(s, " ")
	cmd := exec.Command(args[0], args[1:]...)
	b, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Printf("--------------------------------------\nManual invocation of 'docker build' failed\n--------------------------------------")
	}
	output := string(b[:])
	fmt.Printf("%s\n", output)

	opts := &docker.RunOptions{
		Remove: true,
	}
	output = docker.Run(t, image, opts)
	assert.Contains(t, output, "Docker version 20.10")
	assert.Contains(t, output, "Terraform v0.15.3")
	assert.Contains(t, output, "TFLint version 0.28.1")
	assert.Contains(t, output, "aws-cli/2.2.3")
}
